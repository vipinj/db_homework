#!/usr/bin/python
# fractally distributed numbers

import sys
import math
import random
import numpy as np

def gen_frac(frac, ctr):
    l = [i for i in range(1,ctr+1)]
    p = list(np.random.permutation(l))
    outvec = p
    while len(p) > 1:
        # w/o math.ceil, we get < 100k numbers
        j = int(math.ceil(frac * len(p)))
        #j = int(frac * len(p))
        p = p[0:j]
        outvec += p
    outvec = outvec[0:100000]
    return outvec

def main(frac, ctr):
    outvec = gen_frac(frac, ctr)
    t_l = [i for i in range(1,20001)]
    item_idl = []
    for ii in range(5):
        item_idl += t_l

    print len(item_idl)
    print len(outvec)
    #output_data = zip(item_idl, outvec)

    with open('data','w') as f:
        for j in range(100000):
            f.write('%d %d\n' %(item_idl[j], outvec[j]))
            
if __name__ == '__main__':
    main(0.3, 70000)
    
