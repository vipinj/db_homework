PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
DROP TABLE IF EXISTS tbl1;
CREATE TABLE tbl1(val int);
INSERT INTO "tbl1" VALUES(54);
INSERT INTO "tbl1" VALUES(18);
INSERT INTO "tbl1" VALUES(80);
INSERT INTO "tbl1" VALUES(48);
INSERT INTO "tbl1" VALUES(41);
INSERT INTO "tbl1" VALUES(3);
INSERT INTO "tbl1" VALUES(99);
INSERT INTO "tbl1" VALUES(23);
INSERT INTO "tbl1" VALUES(42);
INSERT INTO "tbl1" VALUES(49);
INSERT INTO "tbl1" VALUES(85);
INSERT INTO "tbl1" VALUES(58);
INSERT INTO "tbl1" VALUES(66);
INSERT INTO "tbl1" VALUES(65);
INSERT INTO "tbl1" VALUES(6);
INSERT INTO "tbl1" VALUES(14);
INSERT INTO "tbl1" VALUES(16);
INSERT INTO "tbl1" VALUES(53);
INSERT INTO "tbl1" VALUES(33);
INSERT INTO "tbl1" VALUES(11);
INSERT INTO "tbl1" VALUES(87);
INSERT INTO "tbl1" VALUES(55);
INSERT INTO "tbl1" VALUES(15);
INSERT INTO "tbl1" VALUES(69);
INSERT INTO "tbl1" VALUES(88);
INSERT INTO "tbl1" VALUES(7);
INSERT INTO "tbl1" VALUES(19);
INSERT INTO "tbl1" VALUES(20);
INSERT INTO "tbl1" VALUES(28);
INSERT INTO "tbl1" VALUES(4);
INSERT INTO "tbl1" VALUES(67);
INSERT INTO "tbl1" VALUES(31);
INSERT INTO "tbl1" VALUES(78);
INSERT INTO "tbl1" VALUES(27);
INSERT INTO "tbl1" VALUES(45);
INSERT INTO "tbl1" VALUES(97);
INSERT INTO "tbl1" VALUES(56);
INSERT INTO "tbl1" VALUES(74);
INSERT INTO "tbl1" VALUES(35);
INSERT INTO "tbl1" VALUES(100);
INSERT INTO "tbl1" VALUES(76);
INSERT INTO "tbl1" VALUES(51);
INSERT INTO "tbl1" VALUES(46);
INSERT INTO "tbl1" VALUES(94);
INSERT INTO "tbl1" VALUES(72);
INSERT INTO "tbl1" VALUES(9);
INSERT INTO "tbl1" VALUES(25);
INSERT INTO "tbl1" VALUES(8);
INSERT INTO "tbl1" VALUES(5);
INSERT INTO "tbl1" VALUES(43);
INSERT INTO "tbl1" VALUES(29);
INSERT INTO "tbl1" VALUES(2);
INSERT INTO "tbl1" VALUES(86);
INSERT INTO "tbl1" VALUES(89);
INSERT INTO "tbl1" VALUES(57);
INSERT INTO "tbl1" VALUES(40);
INSERT INTO "tbl1" VALUES(26);
INSERT INTO "tbl1" VALUES(36);
INSERT INTO "tbl1" VALUES(21);
INSERT INTO "tbl1" VALUES(64);
INSERT INTO "tbl1" VALUES(98);
INSERT INTO "tbl1" VALUES(90);
INSERT INTO "tbl1" VALUES(50);
INSERT INTO "tbl1" VALUES(75);
INSERT INTO "tbl1" VALUES(71);
INSERT INTO "tbl1" VALUES(30);
INSERT INTO "tbl1" VALUES(12);
INSERT INTO "tbl1" VALUES(68);
INSERT INTO "tbl1" VALUES(91);
INSERT INTO "tbl1" VALUES(83);
INSERT INTO "tbl1" VALUES(95);
INSERT INTO "tbl1" VALUES(34);
INSERT INTO "tbl1" VALUES(70);
INSERT INTO "tbl1" VALUES(32);
INSERT INTO "tbl1" VALUES(17);
INSERT INTO "tbl1" VALUES(96);
INSERT INTO "tbl1" VALUES(37);
INSERT INTO "tbl1" VALUES(38);
INSERT INTO "tbl1" VALUES(61);
INSERT INTO "tbl1" VALUES(60);
INSERT INTO "tbl1" VALUES(81);
INSERT INTO "tbl1" VALUES(10);
INSERT INTO "tbl1" VALUES(13);
INSERT INTO "tbl1" VALUES(84);
INSERT INTO "tbl1" VALUES(22);
INSERT INTO "tbl1" VALUES(62);
INSERT INTO "tbl1" VALUES(79);
INSERT INTO "tbl1" VALUES(73);
INSERT INTO "tbl1" VALUES(39);
INSERT INTO "tbl1" VALUES(1);
INSERT INTO "tbl1" VALUES(63);
INSERT INTO "tbl1" VALUES(24);
INSERT INTO "tbl1" VALUES(52);
INSERT INTO "tbl1" VALUES(92);
INSERT INTO "tbl1" VALUES(93);
INSERT INTO "tbl1" VALUES(44);
INSERT INTO "tbl1" VALUES(82);
INSERT INTO "tbl1" VALUES(47);
INSERT INTO "tbl1" VALUES(77);
INSERT INTO "tbl1" VALUES(59);
INSERT INTO "tbl1" VALUES(54);
INSERT INTO "tbl1" VALUES(18);
INSERT INTO "tbl1" VALUES(80);
INSERT INTO "tbl1" VALUES(48);
INSERT INTO "tbl1" VALUES(41);
INSERT INTO "tbl1" VALUES(3);
INSERT INTO "tbl1" VALUES(99);
INSERT INTO "tbl1" VALUES(23);
INSERT INTO "tbl1" VALUES(42);
INSERT INTO "tbl1" VALUES(49);
INSERT INTO "tbl1" VALUES(85);
INSERT INTO "tbl1" VALUES(58);
INSERT INTO "tbl1" VALUES(66);
INSERT INTO "tbl1" VALUES(65);
INSERT INTO "tbl1" VALUES(6);
INSERT INTO "tbl1" VALUES(14);
INSERT INTO "tbl1" VALUES(16);
INSERT INTO "tbl1" VALUES(53);
INSERT INTO "tbl1" VALUES(33);
INSERT INTO "tbl1" VALUES(11);
INSERT INTO "tbl1" VALUES(87);
INSERT INTO "tbl1" VALUES(55);
INSERT INTO "tbl1" VALUES(15);
INSERT INTO "tbl1" VALUES(69);
INSERT INTO "tbl1" VALUES(88);
INSERT INTO "tbl1" VALUES(7);
INSERT INTO "tbl1" VALUES(19);
INSERT INTO "tbl1" VALUES(20);
INSERT INTO "tbl1" VALUES(28);
INSERT INTO "tbl1" VALUES(4);
INSERT INTO "tbl1" VALUES(54);
INSERT INTO "tbl1" VALUES(18);
INSERT INTO "tbl1" VALUES(80);
INSERT INTO "tbl1" VALUES(48);
INSERT INTO "tbl1" VALUES(41);
INSERT INTO "tbl1" VALUES(3);
INSERT INTO "tbl1" VALUES(99);
INSERT INTO "tbl1" VALUES(23);
INSERT INTO "tbl1" VALUES(42);
INSERT INTO "tbl1" VALUES(54);
INSERT INTO "tbl1" VALUES(18);
INSERT INTO "tbl1" VALUES(80);
INSERT INTO "tbl1" VALUES(54);
CREATE INDEX tbl1_idx on tbl1(val);
DROP TABLE IF EXISTS tbl2;
CREATE TABLE tbl2(val int);
INSERT INTO "tbl2" VALUES(69);
INSERT INTO "tbl2" VALUES(14);
INSERT INTO "tbl2" VALUES(62);
INSERT INTO "tbl2" VALUES(18);
INSERT INTO "tbl2" VALUES(98);
INSERT INTO "tbl2" VALUES(6);
INSERT INTO "tbl2" VALUES(31);
INSERT INTO "tbl2" VALUES(72);
INSERT INTO "tbl2" VALUES(100);
INSERT INTO "tbl2" VALUES(40);
INSERT INTO "tbl2" VALUES(73);
INSERT INTO "tbl2" VALUES(83);
INSERT INTO "tbl2" VALUES(21);
INSERT INTO "tbl2" VALUES(63);
INSERT INTO "tbl2" VALUES(66);
INSERT INTO "tbl2" VALUES(90);
INSERT INTO "tbl2" VALUES(81);
INSERT INTO "tbl2" VALUES(8);
INSERT INTO "tbl2" VALUES(36);
INSERT INTO "tbl2" VALUES(53);
INSERT INTO "tbl2" VALUES(23);
INSERT INTO "tbl2" VALUES(89);
INSERT INTO "tbl2" VALUES(79);
INSERT INTO "tbl2" VALUES(68);
INSERT INTO "tbl2" VALUES(97);
INSERT INTO "tbl2" VALUES(29);
INSERT INTO "tbl2" VALUES(17);
INSERT INTO "tbl2" VALUES(57);
INSERT INTO "tbl2" VALUES(84);
INSERT INTO "tbl2" VALUES(20);
INSERT INTO "tbl2" VALUES(27);
INSERT INTO "tbl2" VALUES(44);
INSERT INTO "tbl2" VALUES(93);
INSERT INTO "tbl2" VALUES(45);
INSERT INTO "tbl2" VALUES(60);
INSERT INTO "tbl2" VALUES(94);
INSERT INTO "tbl2" VALUES(87);
INSERT INTO "tbl2" VALUES(34);
INSERT INTO "tbl2" VALUES(50);
INSERT INTO "tbl2" VALUES(70);
INSERT INTO "tbl2" VALUES(10);
INSERT INTO "tbl2" VALUES(54);
INSERT INTO "tbl2" VALUES(61);
INSERT INTO "tbl2" VALUES(1);
INSERT INTO "tbl2" VALUES(12);
INSERT INTO "tbl2" VALUES(42);
INSERT INTO "tbl2" VALUES(95);
INSERT INTO "tbl2" VALUES(51);
INSERT INTO "tbl2" VALUES(49);
INSERT INTO "tbl2" VALUES(47);
INSERT INTO "tbl2" VALUES(78);
INSERT INTO "tbl2" VALUES(35);
INSERT INTO "tbl2" VALUES(85);
INSERT INTO "tbl2" VALUES(11);
INSERT INTO "tbl2" VALUES(91);
INSERT INTO "tbl2" VALUES(77);
INSERT INTO "tbl2" VALUES(59);
INSERT INTO "tbl2" VALUES(67);
INSERT INTO "tbl2" VALUES(24);
INSERT INTO "tbl2" VALUES(22);
INSERT INTO "tbl2" VALUES(82);
INSERT INTO "tbl2" VALUES(96);
INSERT INTO "tbl2" VALUES(15);
INSERT INTO "tbl2" VALUES(19);
INSERT INTO "tbl2" VALUES(32);
INSERT INTO "tbl2" VALUES(46);
INSERT INTO "tbl2" VALUES(13);
INSERT INTO "tbl2" VALUES(4);
INSERT INTO "tbl2" VALUES(48);
INSERT INTO "tbl2" VALUES(25);
INSERT INTO "tbl2" VALUES(28);
INSERT INTO "tbl2" VALUES(75);
INSERT INTO "tbl2" VALUES(39);
INSERT INTO "tbl2" VALUES(41);
INSERT INTO "tbl2" VALUES(43);
INSERT INTO "tbl2" VALUES(86);
INSERT INTO "tbl2" VALUES(65);
INSERT INTO "tbl2" VALUES(33);
INSERT INTO "tbl2" VALUES(16);
INSERT INTO "tbl2" VALUES(71);
INSERT INTO "tbl2" VALUES(76);
INSERT INTO "tbl2" VALUES(26);
INSERT INTO "tbl2" VALUES(2);
INSERT INTO "tbl2" VALUES(58);
INSERT INTO "tbl2" VALUES(38);
INSERT INTO "tbl2" VALUES(30);
INSERT INTO "tbl2" VALUES(80);
INSERT INTO "tbl2" VALUES(52);
INSERT INTO "tbl2" VALUES(37);
INSERT INTO "tbl2" VALUES(88);
INSERT INTO "tbl2" VALUES(56);
INSERT INTO "tbl2" VALUES(9);
INSERT INTO "tbl2" VALUES(92);
INSERT INTO "tbl2" VALUES(7);
INSERT INTO "tbl2" VALUES(99);
INSERT INTO "tbl2" VALUES(3);
INSERT INTO "tbl2" VALUES(55);
INSERT INTO "tbl2" VALUES(5);
INSERT INTO "tbl2" VALUES(64);
INSERT INTO "tbl2" VALUES(74);
INSERT INTO "tbl2" VALUES(69);
INSERT INTO "tbl2" VALUES(14);
INSERT INTO "tbl2" VALUES(62);
INSERT INTO "tbl2" VALUES(18);
INSERT INTO "tbl2" VALUES(98);
INSERT INTO "tbl2" VALUES(6);
INSERT INTO "tbl2" VALUES(31);
INSERT INTO "tbl2" VALUES(72);
INSERT INTO "tbl2" VALUES(100);
INSERT INTO "tbl2" VALUES(40);
INSERT INTO "tbl2" VALUES(73);
INSERT INTO "tbl2" VALUES(83);
INSERT INTO "tbl2" VALUES(21);
INSERT INTO "tbl2" VALUES(63);
INSERT INTO "tbl2" VALUES(66);
INSERT INTO "tbl2" VALUES(90);
INSERT INTO "tbl2" VALUES(81);
INSERT INTO "tbl2" VALUES(8);
INSERT INTO "tbl2" VALUES(36);
INSERT INTO "tbl2" VALUES(53);
INSERT INTO "tbl2" VALUES(23);
INSERT INTO "tbl2" VALUES(89);
INSERT INTO "tbl2" VALUES(79);
INSERT INTO "tbl2" VALUES(68);
INSERT INTO "tbl2" VALUES(97);
INSERT INTO "tbl2" VALUES(29);
INSERT INTO "tbl2" VALUES(17);
INSERT INTO "tbl2" VALUES(57);
INSERT INTO "tbl2" VALUES(84);
INSERT INTO "tbl2" VALUES(20);
INSERT INTO "tbl2" VALUES(69);
INSERT INTO "tbl2" VALUES(14);
INSERT INTO "tbl2" VALUES(62);
INSERT INTO "tbl2" VALUES(18);
INSERT INTO "tbl2" VALUES(98);
INSERT INTO "tbl2" VALUES(6);
INSERT INTO "tbl2" VALUES(31);
INSERT INTO "tbl2" VALUES(72);
INSERT INTO "tbl2" VALUES(100);
INSERT INTO "tbl2" VALUES(69);
INSERT INTO "tbl2" VALUES(14);
INSERT INTO "tbl2" VALUES(62);
INSERT INTO "tbl2" VALUES(69);
DROP TABLE IF EXISTS tbl3;
CREATE TABLE tbl3(val int);
INSERT INTO "tbl3" VALUES(43);
INSERT INTO "tbl3" VALUES(76);
INSERT INTO "tbl3" VALUES(30);
INSERT INTO "tbl3" VALUES(51);
INSERT INTO "tbl3" VALUES(26);
INSERT INTO "tbl3" VALUES(95);
INSERT INTO "tbl3" VALUES(71);
INSERT INTO "tbl3" VALUES(16);
INSERT INTO "tbl3" VALUES(53);
INSERT INTO "tbl3" VALUES(94);
INSERT INTO "tbl3" VALUES(56);
INSERT INTO "tbl3" VALUES(62);
INSERT INTO "tbl3" VALUES(67);
INSERT INTO "tbl3" VALUES(80);
INSERT INTO "tbl3" VALUES(45);
INSERT INTO "tbl3" VALUES(50);
INSERT INTO "tbl3" VALUES(29);
INSERT INTO "tbl3" VALUES(72);
INSERT INTO "tbl3" VALUES(10);
INSERT INTO "tbl3" VALUES(100);
INSERT INTO "tbl3" VALUES(46);
INSERT INTO "tbl3" VALUES(65);
INSERT INTO "tbl3" VALUES(4);
INSERT INTO "tbl3" VALUES(54);
INSERT INTO "tbl3" VALUES(66);
INSERT INTO "tbl3" VALUES(82);
INSERT INTO "tbl3" VALUES(22);
INSERT INTO "tbl3" VALUES(84);
INSERT INTO "tbl3" VALUES(55);
INSERT INTO "tbl3" VALUES(3);
INSERT INTO "tbl3" VALUES(19);
INSERT INTO "tbl3" VALUES(11);
INSERT INTO "tbl3" VALUES(81);
INSERT INTO "tbl3" VALUES(85);
INSERT INTO "tbl3" VALUES(86);
INSERT INTO "tbl3" VALUES(6);
INSERT INTO "tbl3" VALUES(79);
INSERT INTO "tbl3" VALUES(1);
INSERT INTO "tbl3" VALUES(69);
INSERT INTO "tbl3" VALUES(20);
INSERT INTO "tbl3" VALUES(77);
INSERT INTO "tbl3" VALUES(59);
INSERT INTO "tbl3" VALUES(40);
INSERT INTO "tbl3" VALUES(12);
INSERT INTO "tbl3" VALUES(70);
INSERT INTO "tbl3" VALUES(32);
INSERT INTO "tbl3" VALUES(34);
INSERT INTO "tbl3" VALUES(90);
INSERT INTO "tbl3" VALUES(52);
INSERT INTO "tbl3" VALUES(28);
INSERT INTO "tbl3" VALUES(68);
INSERT INTO "tbl3" VALUES(44);
INSERT INTO "tbl3" VALUES(91);
INSERT INTO "tbl3" VALUES(89);
INSERT INTO "tbl3" VALUES(27);
INSERT INTO "tbl3" VALUES(14);
INSERT INTO "tbl3" VALUES(97);
INSERT INTO "tbl3" VALUES(78);
INSERT INTO "tbl3" VALUES(36);
INSERT INTO "tbl3" VALUES(39);
INSERT INTO "tbl3" VALUES(31);
INSERT INTO "tbl3" VALUES(83);
INSERT INTO "tbl3" VALUES(75);
INSERT INTO "tbl3" VALUES(87);
INSERT INTO "tbl3" VALUES(57);
INSERT INTO "tbl3" VALUES(13);
INSERT INTO "tbl3" VALUES(21);
INSERT INTO "tbl3" VALUES(99);
INSERT INTO "tbl3" VALUES(9);
INSERT INTO "tbl3" VALUES(8);
INSERT INTO "tbl3" VALUES(47);
INSERT INTO "tbl3" VALUES(2);
INSERT INTO "tbl3" VALUES(63);
INSERT INTO "tbl3" VALUES(93);
INSERT INTO "tbl3" VALUES(96);
INSERT INTO "tbl3" VALUES(61);
INSERT INTO "tbl3" VALUES(23);
INSERT INTO "tbl3" VALUES(25);
INSERT INTO "tbl3" VALUES(88);
INSERT INTO "tbl3" VALUES(92);
INSERT INTO "tbl3" VALUES(33);
INSERT INTO "tbl3" VALUES(48);
INSERT INTO "tbl3" VALUES(37);
INSERT INTO "tbl3" VALUES(64);
INSERT INTO "tbl3" VALUES(98);
INSERT INTO "tbl3" VALUES(42);
INSERT INTO "tbl3" VALUES(18);
INSERT INTO "tbl3" VALUES(58);
INSERT INTO "tbl3" VALUES(74);
INSERT INTO "tbl3" VALUES(17);
INSERT INTO "tbl3" VALUES(7);
INSERT INTO "tbl3" VALUES(60);
INSERT INTO "tbl3" VALUES(38);
INSERT INTO "tbl3" VALUES(35);
INSERT INTO "tbl3" VALUES(5);
INSERT INTO "tbl3" VALUES(24);
INSERT INTO "tbl3" VALUES(41);
INSERT INTO "tbl3" VALUES(73);
INSERT INTO "tbl3" VALUES(15);
INSERT INTO "tbl3" VALUES(49);
CREATE INDEX tbl3_idx on tbl3(val);
DROP TABLE IF EXISTS tbl4;
CREATE TABLE tbl4(val int);
INSERT INTO "tbl4" VALUES(50);
INSERT INTO "tbl4" VALUES(77);
INSERT INTO "tbl4" VALUES(58);
INSERT INTO "tbl4" VALUES(9);
INSERT INTO "tbl4" VALUES(56);
INSERT INTO "tbl4" VALUES(24);
INSERT INTO "tbl4" VALUES(35);
INSERT INTO "tbl4" VALUES(46);
INSERT INTO "tbl4" VALUES(38);
INSERT INTO "tbl4" VALUES(94);
INSERT INTO "tbl4" VALUES(47);
INSERT INTO "tbl4" VALUES(63);
INSERT INTO "tbl4" VALUES(84);
INSERT INTO "tbl4" VALUES(31);
INSERT INTO "tbl4" VALUES(76);
INSERT INTO "tbl4" VALUES(28);
INSERT INTO "tbl4" VALUES(69);
INSERT INTO "tbl4" VALUES(21);
INSERT INTO "tbl4" VALUES(26);
INSERT INTO "tbl4" VALUES(8);
INSERT INTO "tbl4" VALUES(92);
INSERT INTO "tbl4" VALUES(75);
INSERT INTO "tbl4" VALUES(70);
INSERT INTO "tbl4" VALUES(59);
INSERT INTO "tbl4" VALUES(73);
INSERT INTO "tbl4" VALUES(95);
INSERT INTO "tbl4" VALUES(68);
INSERT INTO "tbl4" VALUES(12);
INSERT INTO "tbl4" VALUES(88);
INSERT INTO "tbl4" VALUES(86);
INSERT INTO "tbl4" VALUES(36);
INSERT INTO "tbl4" VALUES(99);
INSERT INTO "tbl4" VALUES(52);
INSERT INTO "tbl4" VALUES(74);
INSERT INTO "tbl4" VALUES(42);
INSERT INTO "tbl4" VALUES(3);
INSERT INTO "tbl4" VALUES(22);
INSERT INTO "tbl4" VALUES(81);
INSERT INTO "tbl4" VALUES(62);
INSERT INTO "tbl4" VALUES(54);
INSERT INTO "tbl4" VALUES(72);
INSERT INTO "tbl4" VALUES(1);
INSERT INTO "tbl4" VALUES(20);
INSERT INTO "tbl4" VALUES(66);
INSERT INTO "tbl4" VALUES(44);
INSERT INTO "tbl4" VALUES(30);
INSERT INTO "tbl4" VALUES(83);
INSERT INTO "tbl4" VALUES(2);
INSERT INTO "tbl4" VALUES(55);
INSERT INTO "tbl4" VALUES(79);
INSERT INTO "tbl4" VALUES(17);
INSERT INTO "tbl4" VALUES(51);
INSERT INTO "tbl4" VALUES(97);
INSERT INTO "tbl4" VALUES(25);
INSERT INTO "tbl4" VALUES(16);
INSERT INTO "tbl4" VALUES(80);
INSERT INTO "tbl4" VALUES(39);
INSERT INTO "tbl4" VALUES(34);
INSERT INTO "tbl4" VALUES(61);
INSERT INTO "tbl4" VALUES(18);
INSERT INTO "tbl4" VALUES(82);
INSERT INTO "tbl4" VALUES(7);
INSERT INTO "tbl4" VALUES(85);
INSERT INTO "tbl4" VALUES(45);
INSERT INTO "tbl4" VALUES(67);
INSERT INTO "tbl4" VALUES(60);
INSERT INTO "tbl4" VALUES(71);
INSERT INTO "tbl4" VALUES(93);
INSERT INTO "tbl4" VALUES(43);
INSERT INTO "tbl4" VALUES(53);
INSERT INTO "tbl4" VALUES(32);
INSERT INTO "tbl4" VALUES(89);
INSERT INTO "tbl4" VALUES(49);
INSERT INTO "tbl4" VALUES(15);
INSERT INTO "tbl4" VALUES(4);
INSERT INTO "tbl4" VALUES(41);
INSERT INTO "tbl4" VALUES(10);
INSERT INTO "tbl4" VALUES(11);
INSERT INTO "tbl4" VALUES(100);
INSERT INTO "tbl4" VALUES(19);
INSERT INTO "tbl4" VALUES(65);
INSERT INTO "tbl4" VALUES(6);
INSERT INTO "tbl4" VALUES(37);
INSERT INTO "tbl4" VALUES(29);
INSERT INTO "tbl4" VALUES(87);
INSERT INTO "tbl4" VALUES(64);
INSERT INTO "tbl4" VALUES(5);
INSERT INTO "tbl4" VALUES(27);
INSERT INTO "tbl4" VALUES(78);
INSERT INTO "tbl4" VALUES(91);
INSERT INTO "tbl4" VALUES(13);
INSERT INTO "tbl4" VALUES(48);
INSERT INTO "tbl4" VALUES(57);
INSERT INTO "tbl4" VALUES(33);
INSERT INTO "tbl4" VALUES(98);
INSERT INTO "tbl4" VALUES(40);
INSERT INTO "tbl4" VALUES(90);
INSERT INTO "tbl4" VALUES(96);
INSERT INTO "tbl4" VALUES(14);
INSERT INTO "tbl4" VALUES(23);
COMMIT;
