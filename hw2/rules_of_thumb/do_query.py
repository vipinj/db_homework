#!/usr/bin/env python

import os

for tab in ["tbl1", "tbl2", "tbl3", "tbl4"]:
    cmd = "time echo 'select count(*) from %s where val < 1000;' | sqlite3 rules_of_thumb.sqlite3" % tab
    print cmd
    os.system(cmd)
