#!/usr/bin/env python

import sys
import math
import random
import numpy as np

n_rows = 100

def gen_frac(frac, ctr):
    l = [i for i in range(1,ctr+1)]
    p = list(np.random.permutation(l))
    outvec = p
    while len(p) > 1:
        # w/o math.ceil, we get < 100k numbers
        j = int(math.ceil(frac * len(p)))
        #j = int(frac * len(p))
        p = p[0:j]
        outvec += p
    outvec = outvec[0:100000]
    return outvec

def gen_uniform(ctr):
    l = [i for i in range(1,ctr+1)]
    p = list(np.random.permutation(l))
    outvec = p
    outvec = outvec[0:100000]
    return outvec

with open("dump.sql", "w") as f:
    f.write("PRAGMA foreign_keys=OFF;\n")
    f.write("BEGIN TRANSACTION;\n")

    # tbl1: fractal, with index
    f.write("DROP TABLE IF EXISTS tbl1;\n")
    f.write("CREATE TABLE tbl1(val int);\n")
    for v in gen_frac(0.3, n_rows):
        f.write("INSERT INTO \"tbl1\" VALUES(%d);\n" % v);
    f.write("CREATE INDEX tbl1_idx on tbl1(val);\n")
    
    # tbl2: fractal, no index
    f.write("DROP TABLE IF EXISTS tbl2;\n")
    f.write("CREATE TABLE tbl2(val int);\n")
    for v in gen_frac(0.3, n_rows):
        f.write("INSERT INTO \"tbl2\" VALUES(%d);\n" % v);

    # tbl3: uniform, with index
    f.write("DROP TABLE IF EXISTS tbl3;\n")
    f.write("CREATE TABLE tbl3(val int);\n")
    for v in gen_uniform(n_rows):
        f.write("INSERT INTO \"tbl3\" VALUES(%d);\n" % v);
    f.write("CREATE INDEX tbl3_idx on tbl3(val);\n")

    # tbl4: fractal, no index
    f.write("DROP TABLE IF EXISTS tbl4;\n")
    f.write("CREATE TABLE tbl4(val int);\n")
    for v in gen_uniform(n_rows):
        f.write("INSERT INTO \"tbl4\" VALUES(%d);\n" % v);

    f.write("COMMIT;\n")
    pass
